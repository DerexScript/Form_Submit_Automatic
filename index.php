<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<title>Form Submit Automatic</title>
</head>
<body>

	<form name='redirect' action='action.php' method='post'>
		Input: <input type="text" name="nome1" value="value1">
		Input: <input type="text" name="nome2" value="value2">
		Input: <input type="text" name="nome3" value="value3">
		<input type="submit">
	</form>
	
	<!-- Codigo JavaScript responsavel por fazer submit do 'form' com nome 'Redirect' no carregar da pagina! -->
	<script type='text/javascript'>
		document.redirect.submit();
	</script>

</body>
</html>